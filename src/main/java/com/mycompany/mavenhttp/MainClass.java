package com.mycompany.mavenhttp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *
 * @author Admin
 */
public class MainClass {

    private static String codeHtml;
    private static String rokOd = "2018";
    private static String rokDo = "2018";
    private static List lista1 = new ArrayList();
    private static final String SAMPLE_CSV_FILE = "./sample.csv";

    public static void main(String[] args) throws IOException, Exception {
        // Make simple GUI and run generator
        JFrame f = new JFrame(); 
        JTextField tf1 = new JTextField();
        JTextField tf2 = new JTextField();
        JLabel l1, l2, l3;
        l1 = new JLabel("Rok od:");
        l1.setBounds(30, 10, 100, 20);
        l2 = new JLabel("Rok do:");
        l2.setBounds(30, 60, 100, 20);
        l3 = new JLabel("Generator publikacji pracowników UTP");
        l3.setBounds(30, 130, 250, 20);
        f.add(l1);
        f.add(l2);
        f.add(l3);
        tf1.setBounds(30, 30, 100, 30);
        tf1.setText(rokOd);
        tf2.setBounds(30, 80, 100, 30);
        tf2.setText(rokDo);
        JButton b = new JButton("Generuj"); 
        b.setBounds(140, 45, 110, 40); 
        b.addActionListener(e -> {
            try {
                 rokOd=tf1.getText();   
                 rokDo=tf2.getText();          
                run();
            } catch (Exception ex) {
                System.out.print("Nie działa");
            }
        });
        f.add(tf1);
        f.add(tf2);
        f.add(b); 

        f.setSize(300, 200);
        f.setLayout(null); 
        f.setVisible(true); 
    }

    private static void run() throws IOException, Exception {
        
        MainClass http = new MainClass();
        System.out.println("\nTesting 1 - Send Http POST request");
        // GET HTML CODE
        http.sendPost();

        System.out.println("Testing 2 - Find regex");
        // FIRST CLEAR HTML CODE
        http.RegexHtmlCode(codeHtml);

        // SECOND REGEX + FILE GENERATOR
        http.PrintList(lista1);

        System.out.println("END");
    }

    // HTTP POST REQUEST
    private void sendPost() throws Exception {

        String url = "http://www.bg.utp.edu.pl/cgi-bin/expertus/expertus.exe";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest method
        con.setRequestMethod("POST");
        
        //add reuqest header
        String urlParameters = "KAT=%2Fexpertus%2Fpar%2F&FST=data.fst&F_00=02&V_00=&F_01=39&V_01=&F_02=11&V_02=&cond=AND&ekran=ISO&I_XX=a&year00=2&ZA=07&F_07=00&V_07=" + rokOd + "%40" + rokDo + "&year01=" + rokOd + "&year02=" + rokDo + "&F_16=57&F_11=19&V_11=&F_05=46&V_05=&pubidx=%40&F_18=22&F_14=21&F_04=15&B_00=015&C_00=3&D_00=&F_08=17&F_06=27&F_13=26&FDT=data01.fdt&sort=&mask=1";
        
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        codeHtml = response.toString();
        //print result
        //System.out.println(response.toString());

    }

    private void RegexHtmlCode(String str) {
        Pattern pattern = Pattern.compile("checkbox\"\\sVALUE(.*?)</script>");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            lista1.add(matcher.group());
        }
        System.out.println("Ilosc wyszukań: " + lista1.size());
        System.out.println();

    }

    private void PrintList(List<String> list) throws IOException {
        // Wyświetlenie rekordu bez formatowania
        /*
        for (int i = 0; i < 4; i++) {
            System.out.println(list.get(i));
        }
         */

        BufferedWriter writer = Files.newBufferedWriter(Paths.get(SAMPLE_CSV_FILE));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("ID", "Tytuł", "Autorzy", "Czasopismo", "Numer", "Afiliacja", "Doi"));

        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i).replaceAll("\\<(.*?)\\>", "||");
            String str1 = str.replaceAll("ch(.*?)\\>", " ");
            //System.out.println(str1); // wyświetla rekord po formacie

            //System.out.println("LP:" + (i+1));
            System.out.println();

            // Tytuł
            String tytul = "";
            Pattern pattern = Pattern.compile("(?<=Tytuł:\\s\\|{2})(.*?)(?=\\|{4,})");
            Matcher matcher = pattern.matcher(str);
            while (matcher.find()) {
                tytul = matcher.group();

            }
            System.out.println("Tytuł: " + tytul);

            // Autorzy
            String autor = "";
            Pattern pattern1 = Pattern.compile("(?<=Autorzy:\\s\\|{2})(.*?)(?=\\|{4,})");
            Matcher matcher1 = pattern1.matcher(str);
            while (matcher1.find()) {
                autor = matcher1.group();

            }
            autor = autor.replaceAll("\\|", "");
            System.out.println("Autorzy: " + autor);

            // Czasopismo
            String czasopismo = "";
            Pattern pattern2 = Pattern.compile("(?<=Czasopismo:\\s\\|{2})(.*?)(?=\\|{4,})");
            Matcher matcher2 = pattern2.matcher(str);
            while (matcher2.find()) {
                czasopismo = matcher2.group();

            }
            System.out.println("Czasopismo: " + czasopismo);

            // Numer
            String numer = "";
            Pattern pattern3 = Pattern.compile("(?<=p-ISSN:\\s\\|{2})(.*?)(?=\\|{4,})");
            Matcher matcher3 = pattern3.matcher(str);
            Pattern pattern4 = Pattern.compile("(?<=p-ISBN:\\s\\|{2})(.*?)(?=\\|{4,})");
            Matcher matcher4 = pattern4.matcher(str);
            if (matcher3.find()) {
                numer = "ISSN: " + matcher3.group();

            } else if (matcher4.find()) {
                numer = "ISBN: " + matcher4.group();
            } else {
                numer = "ISSN/ISBN - brak";
            }
            System.out.println(numer);

            // Afiliacja
            String afilicja = "";
            Pattern pattern5 = Pattern.compile("Afiliacja pracy na rzecz UTP");
            Matcher matcher5 = pattern5.matcher(str);
            if (matcher5.find()) {
                afilicja = "Afiliacja: TAK";
            } else {
                afilicja = "Afiliacja: NIE";
            }
            System.out.println(afilicja);

            // DOI
            String doi = "";
            Pattern pattern6 = Pattern.compile("(?<=doi_disp\\(\\')(.*?)(?=\\')");
            Matcher matcher6 = pattern6.matcher(str);
            if (matcher6.find()) {
                doi = "DOI: " + matcher6.group();
            } else {
                doi = "DOI: NIE";
            }
            System.out.println(doi);

            System.out.println();

            csvPrinter.printRecord(i + 1, tytul, autor, czasopismo, numer, afilicja, doi);
        }
        csvPrinter.flush();
    }

}
